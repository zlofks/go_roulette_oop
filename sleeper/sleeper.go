package sleeper

import "time"

// Sleeper - sleep
type Sleeper struct {
	time int
}

// Sleep - sleep
func (sleeper *Sleeper) Sleep() {
	time.Sleep(time.Duration(sleeper.time) * time.Second)
}

// SetTime - set time
func (sleeper *Sleeper) SetTime(time int) {
	sleeper.time = time
}
