package main

import (
	"GO/roulette_OOP/cleaner"
	"GO/roulette_OOP/gambler"
	"GO/roulette_OOP/roulette"
	"GO/roulette_OOP/sleeper"
)

var money = 1000
var time = 2

func main() {
	// init roulette
	var rouletteObj roulette.Roulette

	// init gambler
	var gamblerObj gambler.Gambler
	gamblerObj.AddMoney(money)

	// init cleaner
	var cleanerObj cleaner.Cleaner

	// init sleeper
	var sleeperObj sleeper.Sleeper
	sleeperObj.SetTime(time)

	// main cicle
	for gamblerObj.GetMoney() > 0 {
		cleanerObj.CleanScreen()
		gamblerObj.PrintMoney()
		gamblerObj.SetField()
		gamblerObj.SetBet()

		rouletteObj.PlaceBet(gamblerObj.GetBet())
		rouletteObj.SetField(gamblerObj.GetField())
		gamblerObj.AddMoney(rouletteObj.Game())
		sleeperObj.Sleep()

	}
}
