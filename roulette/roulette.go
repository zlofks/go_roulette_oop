package roulette

import (
	"fmt"
	"math/rand"
)

// Roulette - roulette
type Roulette struct {
	bet   int
	field string
}

// Game - roulette game
func (roulette *Roulette) Game() int {
	var sector = rand.Intn(37)

	// 0 - zero, 1-18 - red , 19-36 - black

	if sector > 0 && sector < 18 && roulette.field == "red" || sector >= 19 && roulette.field == "black" {
		fmt.Println("You win !", roulette.bet*2, "$")
		return roulette.bet * 2
	}

	fmt.Println("You lose !")
	return 0
}

// PlaceBet - place a bet
func (roulette *Roulette) PlaceBet(bet int) {
	roulette.bet = bet
}

// SetField - set field for roulette
func (roulette *Roulette) SetField(field string) {
	roulette.field = field
}
