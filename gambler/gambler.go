package gambler

import "fmt"

// Gambler - just dude with money
type Gambler struct {
	money int
	bet   int
	field string
}

// AddMoney - set money
func (gambler *Gambler) AddMoney(money int) {
	gambler.money += money
}

// GetMoney - get money
func (gambler *Gambler) GetMoney() int {
	return gambler.money
}

// PrintMoney - print money
func (gambler *Gambler) PrintMoney() {
	fmt.Println("Your money:", gambler.money, "$")
}

// SetBet - set bet
func (gambler *Gambler) SetBet() {
	var bet int

	fmt.Println("Input your bet: ")
	fmt.Scanln(&bet)

	if gambler.money < bet {
		gambler.bet = gambler.money
		gambler.money = 0
	} else {
		gambler.bet = bet
		gambler.money -= bet
	}
	gambler.PrintBet()
}

// GetBet - get bet
func (gambler Gambler) GetBet() int {
	return gambler.bet
}

// PrintBet - print bet
func (gambler *Gambler) PrintBet() {
	fmt.Println("Your bet:", gambler.bet)
}

// SetField - set filed - red or black
func (gambler *Gambler) SetField() {
	fmt.Println("Enter field - red or black: ")
	var field string
	fmt.Scanln(&field)
	gambler.field = field
}

// GetField - get field
func (gambler *Gambler) GetField() string {
	return gambler.field
}
