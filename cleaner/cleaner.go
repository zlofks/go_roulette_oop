package cleaner

import "fmt"

// Cleaner - clear screen
type Cleaner struct {
}

// CleanScreen - clear screen
func (cleaner *Cleaner) CleanScreen() {
	fmt.Println("\033[H\033[2J")
}
